from django.urls import path
from . import views


urlpatterns = [
    path('settings', views.settings, name='settings'),
    path('editProfileForm', views.editProfileForm, name='editProfileForm'),
    path('', views.index, name='index'),
    path('downloadUpdatefile', views.downloadUpdatefile, name='downloadUpdatefile'),
    path('referals', views.referals, name='referals'),
    path('invite', views.invite, name='invite'),
    path('registration', views.registration, name='registration'),
    path('robots', views.robots, name='robots'),
    path('personal_area', views.personal_area, name='personal_area'),
    path('downloadLastRobot', views.downloadLastRobot, name='downloadLastRobot'),
    path('downloadProfile', views.downloadProfile, name='downloadProfile'),
    path('agreement', views.agreement, name='agreement'),
    path('out_balance', views.out_balance, name='out_balance'),
    path('get_settings', views.get_settings, name='get_settings'),
    path('checkLicense', views.checkLicense, name='checkLicense'),
]