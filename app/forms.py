from django import forms

class EditProfile(forms.Form):
    NAME = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Имя пользователя'}), max_length=100, min_length=1)
    #EMAIL = forms.EmailField()
    TELEPHONE = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Телефон'}), max_length=100, min_length=5)
    #PERCENT = forms.IntegerField(widget=forms.NumberInput(attrs={'type': 'range', 'min': 0, 'max': 100, 'onchange':'document.getElementById("percent_input_number").value=this.value;', 'oninput':'document.getElementById("percent_input_number").value=this.value;'}))
    PASSWORD = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Введите новый пароль'}), required=False)
    PASSWORD_OLD = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Введите старый пароль'}), required=False)


class RegistrationForm(forms.Form):
    EMAIL = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control form-control-input', 'placeholder': 'Электронная почта'}))
    PASSWORD = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Пароль'}), required=False)
    ID_PARTNER = forms.CharField(required=False)


class CashBackOutForm(forms.Form):
    SUM = forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control', 'placeholder': 'Сумма'}))
    CARD = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Карта'}))


class CheckAgreementForm(forms.Form):
    AGREE = forms.BooleanField(widget=forms.CheckboxInput(attrs={'style':'width:20px; height:20px; position: relative; top: 6px;'}))


