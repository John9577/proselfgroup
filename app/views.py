from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from app.models import License, OutBalance, Pair
from app.forms import EditProfile, RegistrationForm, CashBackOutForm, CheckAgreementForm
from django.contrib.auth.models import User
from cryptos import *
from django.db.models import Max, Min, Avg
import time

def checkAgreement(request):
    yes_agreement = True
    if request.user.is_authenticated:
        u = User.objects.get(username=request.user)
        yes_agreement = u.profile.yes_agreement
    return yes_agreement


def index(request):
    return render(
        request,
        'index.html',
        context={
                 },
    )


def agreement(request):
    AGREE = request.GET.get('AGREE')
    yes_agreement = False
    if AGREE:
        u = User.objects.get(username=request.user)
        u.profile.yes_agreement = True
        u.save()
        return HttpResponseRedirect(reverse('personal_area'))
    if request.user.is_authenticated:
        u = User.objects.get(username=request.user)
        yes_agreement = u.profile.yes_agreement
    checkAgreementForm = CheckAgreementForm()
    return render(
        request,
        'agreement.html',
        context={
            'checkAgreementForm': checkAgreementForm,
            'yes_agreement': yes_agreement
                 },
    )


def referals(request):
    if request.user.is_authenticated:
        if checkAgreement(request) is False:
            return HttpResponseRedirect(reverse('agreement'))
        partners = []
        for i in User.objects.filter():
            if i.profile.partner_id == request.user.profile.hash_id:
                partners.append(i)
        return render(
            request,
            'referals.html',
            context={
                'partners': partners
                     },
        )
    else:
        return HttpResponseRedirect(reverse('login'))

def invite(request):
    id = request.GET.get('id')
    registrationForm = RegistrationForm()
    registrationForm.fields['ID_PARTNER'].initial = id
    return render(
        request,
        'invite.html',
        context={
            'registrationForm': registrationForm
                 },
    )


def registration(request):
    ID_PARTNER = request.POST['ID_PARTNER']
    EMAIL = request.POST['EMAIL']
    PASSWORD = request.POST['PASSWORD']
    user = User.objects.create_user(EMAIL, EMAIL, PASSWORD)
    user.profile.partner_id = ID_PARTNER
    user.profile.hash_id = sha256(random_string(10))[:8]
    user.save()
    return HttpResponseRedirect(reverse('settings'))


def robots(request):
    if checkAgreement(request) is False:
        return HttpResponseRedirect(reverse('agreement'))
    return render(
        request,
        'robots.html',
        context={
                 },
    )


def personal_area(request):
    if checkAgreement(request) is False:
        return HttpResponseRedirect(reverse('agreement'))
    if request.user.is_authenticated:
        cashBackOutForm = CashBackOutForm()
        licenses = License.objects.filter(PARTNER=request.user)
        partner_all = User.objects.filter()
        balance_out_all = 0
        for i in partner_all:
            if i.profile.partner_id == request.user.profile.hash_id:
                balance_out_all += int(i.profile.balance_out)
        balance_out_all_2 = int(balance_out_all) + int(request.user.profile.balance_out)

        balance_all = 0
        profit_all_day = 0
        profit_all_week = 0
        profit_all_month = 0
        profit_all_all = 0
        for i in licenses:
            balance_all += float(i.BALANCE)
            profit_all_day += float(i.PROFITTODAY)
            profit_all_week += float(i.PROFITWEEK)
            profit_all_month += float(i.PROFITMONTH)
            profit_all_all += float(i.PROFITALL)


        return render(
            request,
            'personal_area.html',
            context={
                'licenses': licenses,
                'balance_out_all': balance_out_all,
                'balance_out_all_2': balance_out_all_2,
                'cashBackOutForm': cashBackOutForm,

                     },
        )
    else:
        return HttpResponseRedirect(reverse('login'))


def downloadLastRobot(request):
    res = HttpResponse(open('files_download/Middle PS Robot.ex4', 'rb'))
    res['Content-Disposition'] = 'attachment; filename=Middle PS Robot.ex4'
    res['Content-Type'] = 'file'
    return res

def downloadProfile(request):
    res = HttpResponse(open('files_download/ProSelf Profile.zip', 'rb'))
    res['Content-Disposition'] = 'attachment; filename=ProSelf Profile.zip'
    res['Content-Type'] = 'file'
    return res


def downloadUpdatefile(request):
    res = HttpResponse(open('files_download/ProSelfInstall.exe', 'rb'))
    res['Content-Disposition'] = 'attachment; filename=ProSelfInstall.exe'
    res['Content-Type'] = 'file'
    return res


def settings(request):
    if checkAgreement(request) is False:
        return HttpResponseRedirect(reverse('agreement'))
    if request.user.is_authenticated:
        editProfile = EditProfile()
        editProfile.fields['NAME'].initial = request.user.profile.name
        editProfile.fields['TELEPHONE'].initial = request.user.profile.telephone_number
        return render(
            request,
            'settings.html',
            context={
                'editProfile': editProfile,
            },
        )
    else:
        return HttpResponseRedirect(reverse('login'))


def editProfileForm(request):
    if request.user.is_authenticated:
        u = User.objects.get(username=request.user)
        u.profile.name = request.POST['NAME']
        u.profile.telephone_number = request.POST['TELEPHONE']
        if u.check_password(request.POST['PASSWORD_OLD']):
            u.set_password(request.POST['PASSWORD'])
        u.save()
    return HttpResponseRedirect(reverse('settings'))


def out_balance(request):
    SUM = request.POST['SUM']
    CARD = request.POST['CARD']
    u = User.objects.get(username=request.user)
    u.profile.free_balance = u.profile.free_balance - int(SUM)
    u.profile.cash_back = u.profile.cash_back + int(SUM)
    u.save()
    OutBalance.objects.create(PARTNER=request.user,
                              SUM=SUM,
                              CARD=CARD)
    return HttpResponseRedirect(reverse('personal_area'))


def get_settings(request):
    NUMBER = request.GET.get('NUMBER')
    BALANCE = request.GET.get('BALANCE')
    PROFIT = request.GET.get('PROFIT')
    MARGIN = request.GET.get('MARGIN')
    COUNTPOSUSDCHF = request.GET.get('COUNTPOSUSDCHF')
    COUNTPOSEURUSD = request.GET.get('COUNTPOSEURUSD')
    COUNTPOSUSDJPY = request.GET.get('COUNTPOSUSDJPY')
    COUNTPOSEURCHF = request.GET.get('COUNTPOSEURCHF')
    COUNTPOSUSDCAD = request.GET.get('COUNTPOSUSDCAD')
    COUNTPOSEURJPY = request.GET.get('COUNTPOSEURJPY')
    PROFITUSDCHF = request.GET.get('PROFITUSDCHF')
    PROFITEURUSD = request.GET.get('PROFITEURUSD')
    PROFITUSDJPY = request.GET.get('PROFITUSDJPY')
    PROFITEURCHF = request.GET.get('PROFITEURCHF')
    PROFITUSDCAD = request.GET.get('PROFITUSDCAD')
    PROFITEURJPY = request.GET.get('PROFITEURJPY')
    PROFITTODAY = request.GET.get('PROFITTODAY')
    PROFITWEEK = request.GET.get('PROFITWEEK')
    PROFITMONTH = request.GET.get('PROFITMONTH')
    PROFITALL = request.GET.get('PROFITALL')
    l = License.objects.filter(NUMBER=NUMBER)
    if len(l) > 0:
        l = l[0]
        l.BALANCE = f"{float(BALANCE):.{2}f}"
        l.PROFIT = f"{float(PROFIT):.{2}f}"
        l.PROFITPERCENT = f"{float(PROFIT) / float(BALANCE) * 100:.{2}f}"
        l.MARGIN = f"{float(MARGIN):.{2}f}"
        l.COUNTPOSUSDCHF = COUNTPOSUSDCHF
        l.COUNTPOSEURUSD = COUNTPOSEURUSD
        l.COUNTPOSUSDJPY = COUNTPOSUSDJPY
        l.COUNTPOSEURCHF = COUNTPOSEURCHF
        l.COUNTPOSUSDCAD = COUNTPOSUSDCAD
        l.COUNTPOSEURJPY = COUNTPOSEURJPY
        l.PROFITUSDCHF = f"{float(PROFITUSDCHF) / float(BALANCE) * 100:.{2}f}"
        l.PROFITEURUSD = f"{float(PROFITEURUSD) / float(BALANCE) * 100:.{2}f}"
        l.PROFITUSDJPY = f"{float(PROFITUSDJPY) / float(BALANCE) * 100:.{2}f}"
        l.PROFITEURCHF = f"{float(PROFITEURCHF) / float(BALANCE) * 100:.{2}f}"
        l.PROFITUSDCAD = f"{float(PROFITUSDCAD) / float(BALANCE) * 100:.{2}f}"
        l.PROFITEURJPY = f"{float(PROFITEURJPY) / float(BALANCE) * 100:.{2}f}"
        l.PROFITTODAY = f"{float(PROFITTODAY):.{2}f}"
        l.PROFITWEEK = f"{float(PROFITWEEK):.{2}f}"
        l.PROFITMONTH = f"{float(PROFITMONTH):.{2}f}"
        l.PROFITALL = f"{float(PROFITALL):.{2}f}"
        l.save()
        return HttpResponse(NUMBER)
    return HttpResponse(0)


def checkLicense(request):
    NUMBER = request.GET.get('NUMBER')
    ID = request.GET.get('ID')
    PAIR = request.GET.get('PAIR')
    l = License.objects.filter(NUMBER=NUMBER)
    if len(l) > 0:
        if l[0].PARTNER.profile.hash_id != ID:
            return HttpResponse(0)
        p = Pair.objects.filter(PAIR=PAIR, LICENSE=l[0])
        if len(p) == 0:
            Pair.objects.create(LICENSE=l[0],
                                PAIR=PAIR)
            mode_buy = 0
            mode_sell = 0
        else:
            mode_buy = p[0].MODE_BUY
            mode_sell = p[0].MODE_SELL
        if l[0].PARTNER.profile.free_balance <= 0:
            mode_buy = 0
            mode_sell = 0
        return HttpResponse(NUMBER+' '+ID+' '+str(mode_buy)+' '+str(mode_sell))
    return HttpResponse(0)