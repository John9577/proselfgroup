from django.contrib import admin

# Register your models here.

from .models import Profile, License, OutBalance, Pair

admin.site.register(Profile)
admin.site.register(License)
admin.site.register(OutBalance)
admin.site.register(Pair)