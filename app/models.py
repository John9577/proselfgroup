from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
import datetime


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, help_text='Enter name', default='')
    telephone_number = models.CharField(max_length=20, help_text='Enter telephone number', default='', null=True, blank=True)
    hash_id = models.CharField(max_length=20, help_text='Enter id', default='')
    partner_id = models.CharField(max_length=20, help_text='Partner id', default='', null=True, blank=True)
    free_balance = models.IntegerField(help_text='Free Balance', default='0')
    balance_out = models.IntegerField(help_text='Out Balance', default='0')
    cash_back = models.IntegerField(help_text='Cashback', default='0')
    number_purchases = models.IntegerField(help_text='Number of purchases', default='0')
    date = models.DateField(help_text='Date', default=datetime.date.today)
    yes_agreement = models.BooleanField(help_text='Agreement', default=False)
    city = models.CharField(max_length=20, help_text='City', default='', null=True, blank=True)
    tg_id = models.CharField(max_length=20, help_text='Telegram id', default='', null=True, blank=True)


    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    # Metadata
    class Meta:
        ordering = ['-id']

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of MyModelName."""
        return reverse('model-detail-view', args=[str(self.id)])

    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return str(self.user.username)


class License(models.Model):
    PARTNER = models.ForeignKey(User, on_delete=models.CASCADE)
    DATE = models.DateField(help_text='Date', default=datetime.date.today)
    NUMBER = models.CharField(max_length=8, help_text="Number")
    SUM = models.IntegerField(help_text="Sum", default=0)
    BALANCE = models.CharField(max_length=10, help_text="Balance", default="", null=True, blank=True)
    PROFIT = models.CharField(max_length=10, help_text="Profit", default="", null=True, blank=True)
    PROFITPERCENT = models.CharField(max_length=10, help_text="Profit %", default="", null=True, blank=True)
    MARGIN = models.CharField(max_length=10, help_text="Margin", default="", null=True, blank=True)
    #MODE = models.IntegerField(help_text="Mode", default=1)
    MAXBUYUSDCHF = models.IntegerField(help_text="Max step BUY USDCHF", default=5)
    MAXBUYEURUSD = models.IntegerField(help_text="Max step BUY EURUSD", default=5)
    MAXBUYUSDJPY = models.IntegerField(help_text="Max step BUY USDJPY", default=5)
    MAXBUYEURCHF = models.IntegerField(help_text="Max step BUY EURCHF", default=5)
    MAXBUYUSDCAD = models.IntegerField(help_text="Max step BUY USDCAD", default=5)
    MAXBUYEURJPY = models.IntegerField(help_text="Max step BUY EURJPY", default=5)
    MAXSELLUSDCHF = models.IntegerField(help_text="Max step SELL USDCHF", default=5)
    MAXSELLEURUSD = models.IntegerField(help_text="Max step SELL EURUSD", default=5)
    MAXSELLUSDJPY = models.IntegerField(help_text="Max step SELL USDJPY", default=5)
    MAXSELLEURCHF = models.IntegerField(help_text="Max step SELL EURCHF", default=5)
    MAXSELLUSDCAD = models.IntegerField(help_text="Max step SELL USDCAD", default=5)
    MAXSELLEURJPY = models.IntegerField(help_text="Max step SELL EURJPY", default=5)
    COUNTPOSUSDCHF = models.IntegerField(help_text="Count step BUY/SELL USDCHF", default=0)
    COUNTPOSEURUSD = models.IntegerField(help_text="Count step BUY/SELL EURUSD", default=0)
    COUNTPOSUSDJPY = models.IntegerField(help_text="Count step BUY/SELL USDJPY", default=0)
    COUNTPOSEURCHF = models.IntegerField(help_text="Count step BUY/SELL EURCHF", default=0)
    COUNTPOSUSDCAD = models.IntegerField(help_text="Count step BUY/SELL USDCAD", default=0)
    COUNTPOSEURJPY = models.IntegerField(help_text="Count step BUY/SELL EURJPY", default=0)
    PROFITTODAY = models.CharField(max_length=10, help_text="Profit today", default="", null=True, blank=True)
    PROFITWEEK = models.CharField(max_length=10, help_text="Profit week", default="", null=True, blank=True)
    PROFITMONTH = models.CharField(max_length=10, help_text="Profit month", default="", null=True, blank=True)
    PROFITALL = models.CharField(max_length=10, help_text="Profit all", default="", null=True, blank=True)
    PROFITUSDCHF = models.CharField(max_length=10, help_text="Profit USDCHF", default="", null=True, blank=True)
    PROFITEURUSD = models.CharField(max_length=10, help_text="Profit EURUSD", default="", null=True, blank=True)
    PROFITUSDJPY = models.CharField(max_length=10, help_text="Profit USDJPY", default="", null=True, blank=True)
    PROFITEURCHF = models.CharField(max_length=10, help_text="Profit EURCHF", default="", null=True, blank=True)
    PROFITUSDCAD = models.CharField(max_length=10, help_text="Profit USDCAD", default="", null=True, blank=True)
    PROFITEURJPY = models.CharField(max_length=10, help_text="Profit EURJPY", default="", null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(License, self).save(*args, **kwargs)
            u = User.objects.get(username=self.PARTNER)
            l = License.objects.filter(PARTNER=self.PARTNER)
            sum = 0
            c = 0
            for i in l:
                sum += i.SUM
                c += 1
            u.profile.balance_out = sum
            u.profile.number_purchases = c
            u.save()
            if u.profile.partner_id != "" and self.SUM > 20000:
                cash_back = self.SUM - 20000
                for i in User.objects.filter():
                    if i.profile.hash_id == u.profile.partner_id:
                        u2 = i
                        u2.profile.free_balance += cash_back
                        u2.save()
                        break
        else:
            super(License, self).save(*args, **kwargs)


    def get_absolute_url(self):
        """
        Returns the url to access a particular instance of MyModelName.
        """
        return reverse('model-detail-view', args=[str(self.NUMBER)])

    def __str__(self):
        """
        String for representing the MyModelName object (in Admin site etc.)
        """
        return str(self.NUMBER)


class Pair(models.Model):
    LICENSE = models.ForeignKey(License, on_delete=models.CASCADE)
    PAIR = models.CharField(max_length=10, help_text="Pair")
    MODE_BUY = models.IntegerField(help_text="Mode buy", default=0)
    MODE_SELL = models.IntegerField(help_text="Mode buy", default=0)


class OutBalance(models.Model):
    PARTNER = models.ForeignKey(User, on_delete=models.CASCADE)
    SUM = models.IntegerField(max_length=10, help_text="Sum", default=0)
    CARD = models.CharField(max_length=20, help_text="Card")
    FINISH = models.BooleanField(help_text='Finish out', default=False)
    DATE = models.DateField(help_text='Date', default=datetime.date.today)

    def get_absolute_url(self):
        """
        Returns the url to access a particular instance of MyModelName.
        """
        return reverse('model-detail-view', args=[str(self.PARTNER)])

    def __str__(self):
        """
        String for representing the MyModelName object (in Admin site etc.)
        """
        return str(self.PARTNER)