from django import template
register = template.Library()

@register.filter
def mod10(value):
    return value % 10


@register.filter
def percent(value, arg):
    if value and arg and (arg != value):
        return f"{float(float(value) / (float(arg) - float(value)) * 100):.{2}f}"
